---
layout: post
title: "Improving Open Development for Everyone"
date: 2015-12-16
categories:
author: Job van der Voort
author_twitter: Jobvo
---

We want to make it super easy for anyone to contribute to GitLab.

That's why we've changed two things:

1. Feature proposals are now handled in the GitLab projects on GitLab.com.
1. We've added the `up-for-grabs` label to relatively simple issues, for anyone to work on.

<!-- more -->

## Feature Proposals

In the past we've used Uservoice on feedback.gitlab.com to handle
feature proposals and voting. This has served us well, but we always felt that
we could bring the discussion even closer to the development of GitLab.

As we've introduced [Award Emoji with GitLab 8.2](https://about.gitlab.com/2015/11/22/gitlab-8-2-released/)
and are extending this to allow
for [sorting, filtering](https://gitlab.com/gitlab-org/gitlab-ce/issues/3672)
based on these, we feel now is the time to start migrating
the feature proposals to GitLab.com.

This means that if you want to propose a feature for GitLab CE or EE,
you can now simply create an issue in their respective projects`.
This will allow anyone to vote, comment and discuss this.

This system is even more useful because anyone that has shown interest in the issue will be automatically updated
with the status of the issue once someone starts working on it. In time this will also
reveal areas where we might want to improve GitLab's issue tracker.

- [GitLab CE issues](https://gitlab.com/gitlab-org/gitlab-ce/issues)
- [GitLab EE issues](https://gitlab.com/gitlab-org/gitlab-ee/issues)

## Up For Grabs

The `up-for-grabs` label was created so that anyone can help make GitLab better
for everyone.

We will label issues that are not too hard or too big to work on.
Whether you're a first-time contributor, someone that wants to improve their
Ruby / Javascript / Go or someone that wants to contribute, but doesn't have time
for a big bug fix or feature, these are issues that you should be able to work on.
If you're already good at the languages GitLab uses, but you are new
to the project, these issues are perfect for you as well.

Have a look at the [up-for-grabs issues for GitLab CE on GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&assignee_id=&author_id=&milestone_title=&label_name=up-for-grabs)
right now. We will add more over time.

Please follow our [the contribution guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md) to help you get started.

## How can we make it easier?

We want to give everyone that loves GitLab the opportunity to help shape its progress and be empowered to do this effortlessly.

How can we make this easier for you? We'd love to hear it in the comments.
