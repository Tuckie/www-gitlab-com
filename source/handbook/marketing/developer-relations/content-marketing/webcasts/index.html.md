---
layout: markdown_page
title: "Content Marketing - GitLab Webcasts"
---

## What is the webcast program at GitLab?

-   The webcast program consists of regular online events for GitLab users and community members.
-   The webcasts are recorded and distributed to GitLab users, and can be referred to in the Resource Library as part of GitLab University

- [How to Schedule a webcast](#schedule)
- [Technical requirements to view a webcast](#view)


## Monthly Program

This program is in development. For the first month, January, we'll start with one webcast, then add another and build up to the full schedule.

### Welcome to GitLab - 20 mins (coming soon!)

-   Live demo run.
-   Similar base demo script each month.
    -   How to use GitLab - essential demo.
    -   This month’s Q+A (commonly asked questions, recent questions from twitter)
    -   GitLab’s products and services.
    -   How to find stuff in the GitLab; getting help, direction, participation.
    -   Community welcome mat: How to meet other GitLab users what events we’ll be at, how about you?
-   Aimed at developers or decision makers who have signed up in the last 30 days, inviting them into the community.

### Release Party - 30 - 40 mins

-   Monthly Thursday following a release.
-   Present highlights from the new features.
-   Refer to any resources, docs, screencasts, etc.
-   Guest speakers from the dev team about the new features.
-   Highlight contributors and the MVP for that month.
-   New contributors welcomed.
-   Q+A from audience.

### GitLab Tutorials - 40 mins - 1 hour

-   Live presentation, demo or discussion on monthly in-depth learning theme.
-   Preceded by 3 weeks of blog posts, screencasts, tutorial and an invitation which leads to the online event.
-   Live event includes: Guest speaker / interview / presentation / demo as appropriate to topic.
-   Q+A from audience. Survey to GitLab users on the topic if appropriate.
-   After event: Blog post of findings from the Q+A, results of survey.
-   Roll-up content into an downloadable ebook, course or other way to make the content more easily accessed and reviewed.

## <a name="schedule"></a>Scheduling webcasts

- Webcasts are on Thursdays, 17:00 UTC (9am PST, 6pm CET)
- Panelists should arrive 15 mins before the webcast for a final sound check
- Panelists should participate in a rehearsal before the webcast

#### Webcast configuration checklist

- Set up webinar in Gotowebinar by cloning the most recent webcast.
- Add a welcome message to attendees
- Upload any handouts (up to 5)
- Upload images, branding and speaker photos (must be jpg or gif and the exact dimensions)

#### Create a calendar event

GoToWebinar doesn't send out a Google calendar friendly invite

- Create an invitation for speakers in Google calendar
- Include their actual URL from their invite.
- Do the same for the rehearsal invitation.


#### Create the program in Marketo

- Clone the last campaign
- Connect the webcast campaign to Gotowebinar. Webcast > Event Settings > Event partner
- Correct version number and invitation date and details for assets:
    -   The landing page
    -   The confirmation email
- Update the hero form on the landing page to the correct form.
- Final check: If the lightbulb is not "on" (yellow), then it's not doing anything.
Check the smart list and flow first. To activate: Click the "registered" smart campaign -> "Schedule" tab -> "Activate" button

#### Promote

- Publish to Facebook
- Schedule tweets
- Create a blog post
- Add to next newsletter

#### As the event starts

- Promote [the appropriate attendees to panelist](https://support.citrixonline.com/en_US/webinar/knowledge_articles/000027765)
- Conduct a sound check and sharing check for anyone who will present.
- Organizers and Panelists are listed in the "Staff" tab and they can mute and unmute attendees, and see questions sent to the Panelists, etc.

## After the webcast

- Process the recording as .mov
- Upload to YouTube and slides to Speakerdeck
- Blog post to share the recording and slides


### <a name="view"></a>Viewing webcasts

- [Citrix Online system requirements](https://support.citrixonline.com/webinar/all_files/G2W010003)
- Using GoToWebinar Instant Join, Linux/Unbuntu users can view in a web browser.
- Linux users should use Chromium to view the browser.
